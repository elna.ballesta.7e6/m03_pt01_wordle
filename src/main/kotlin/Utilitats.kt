/**
 * Funció que posa totes les lletres del teclat en el color inicial
 * @param teclat Array amb totes les lletres del teclat amb el color actual que tenen
 * @return Nou teclat amb les lletres amb color
 */
fun reiniciarTeclat(teclat: Array<String>): Array<String>{
    for (l in 0..teclat.size - 1){
        teclat[l] = colors["negre"].plus(teclat[l].substring(teclat[l].length-3))
    }
    return (teclat)
}

/**
 * Funció que compara la paraula amb l'intent i modifica els colors de jugada i teclat
 * @param paraula String amb la paraula que s'ha d'endivinar
 * @param intent String amb la paraula que s'ha escrit com a intent
 * @param intents Nombre d'intents amb els quals s'ha endevinat la paraula
 * @param jugadaTeclat Parell amb la variable jugada i teclat
 * @return Parell amb la jugada i el teclat actualitzats
 */
fun comprovarParaula(paraula: String, intent:String, intents:Int, jugadaTeclat: Pair<Array<Array<String>>,Array<Array<String>>>): Pair<Array<Array<String>>, Array<Array<String>>> {
    var j = 0
    var copia = paraula
    for (c in intent) {
        if (c == paraula[j]) {
            jugadaTeclat.second[intents][j] = colors["verd"].plus(" ").plus(c).plus(" ")
            copia = copia.substring(0, j).plus(".").plus(copia.substring(j + 1))
            jugadaTeclat.first[0] = canviarColorLletraTeclat(jugadaTeclat.first[0], c, "verd")
        }
        j++
    }
    j = 0
    for (c in intent) {
        if (copia.contains(c) && c != paraula[j]) {
            jugadaTeclat.second[intents][j] = colors["groc"].plus(" ").plus(c).plus(" ")
            var k = 0
            while (copia[k] != c) {
                k++
            }
            copia = copia.substring(0, k).plus(".").plus(copia.substring(k+1))
            jugadaTeclat.first[0] = canviarColorLletraTeclat(jugadaTeclat.first[0], c, "groc")
        }
        else if (c != paraula[j]){
            jugadaTeclat.second[intents][j] = colors["gris"].plus(" ").plus(c).plus(" ")
            jugadaTeclat.first[0] = canviarColorLletraTeclat(jugadaTeclat.first[0], c, "gris")
        }
        j++
    }
    return jugadaTeclat
}

/**
 * Funció que canvia, si cal, el color d'una lletra del teclat
 * @param teclat Array amb totes les lletres del teclat amb el color actual que tenen
 * @param lletra Caràcter que s'ha de comprovar
 * @param colorAPintar String del color amb el qual s'ha de pintar la lletra
 * @return Nou teclat amb la lletra canviada de color
 */
fun canviarColorLletraTeclat(teclat: Array<String>, lletra:Char, colorAPintar:String): Array<String> {
    for (i in 0..teclat.size - 1) {
        if (lletra == teclat[i][teclat[i].length-2]) {
            if (colorAPintar=="verd") {
                teclat[i] = colors["verd"].plus(teclat[i].substring(teclat[i].length-3))
            }
            else if (colorAPintar=="groc"){
                if (teclat[i].substring(0, teclat[i].length-3) != colors["verd"]) teclat[i] = colors["groc"].plus(teclat[i].substring(teclat[i].length-3))
            }
            else {
                if (teclat[i].substring(0, teclat[i].length-3) != colors["verd"] && teclat[i].substring(0, teclat[i].length-3) != colors["groc"]) {
                    teclat[i] = colors["gris"].plus(teclat[i].substring(teclat[i].length-3))
                }
            }
        }
    }
    return (teclat)
}

/**
 * Funció que, si hi ha paraules disponibles, pregunta a l'usuari si vol tornar a jugar
 * @param paraules Llista de paraules que poden sortir per endevinar
 * @param missatges Llista que guarda els missatges que s'imprimeixen durant el programa
 * @return Resposta de l'usuari sobre si vol tornar a jugar
 */
fun preguntarTornarAJugar(paraules:List<String>, missatges: List<String>): String{
    val tornarJugar:String = if (paraules.isEmpty()) {
        println("\n${missatges[13]}")
        "n"
    }
    else {
        println("\n${missatges[14]}")
        readLine().toString()
    }
    return(tornarJugar)
}

/**
 * Funció que imprimeix el tauler del joc amb tots els intents de paraules provades
 * @param teclat Array amb totes les lletres del teclat amb el color actual que tenen
 * @param jugada Matriu amb tots els intents de paraules provades
 * @param missatges Llista que guarda els missatges que s'imprimeixen durant el programa
 */
fun imprimirTaulerJoc(teclat:Array<String>, jugada:Array<Array<String>>, missatges: List<String>){
    for (i in 0..30) println("\n")
    println("         ${missatges[0]}\n")
    for (i in jugada) {
        print("       ")
        for (j in i) {
            print(j)
        }
        print(colors["reset"] + "\n")
    }
    print("\n")
    for (i in 0..teclat.size - 1){
        if (i == 20) print("    ")
        print(teclat[i])
        if (i == 19 || i == 9 || i == 26) println(colors["reset"] + " ")
    }
}

/**
 * Funció que imprimeix el missatge final de la partida
 * @param encertat Si la paraula s'ha encertat o no
 * @param paraula String amb la paraula que s'ha d'endivinar
 * @param missatges Llista que guarda els missatges que s'imprimeixen durant el programa
 */
fun imprimirMissatgesFinalPartida(encertat:Boolean, paraula:String, missatges:List<String>){
    if (encertat) {
        println("\n" + colors["verd"] + "         ${missatges[15]}         " + colors["reset"] + "\n")
    }
    else {
        println("\n" + colors["negreFons"] + "        ${missatges[16]} " + paraula + "        " + colors["reset"] + "\n")
    }
}

/**
 * Funció que imprimeix les estadístiques de la partida
 * @param jugades Nombre de paraules endevinades en x intents. Sent x cada posició de l'array
 * @param ratxaActual Nombre amb la ratxa actual de partides jugades
 * @param ratxaMax Nombre amb la ratxa màxima de partides guanyades
 * @param intents Nombre d'intents amb els quals s'ha endevinat la paraula
 * @param encertat Si la paraula s'ha encertat o no
 * @param missatges Llista que guarda els missatges que s'imprimeixen durant el programa
 */
fun imprimirEstadistiques(jugades:Array<Double>, ratxaActual:Int, ratxaMax:Int, intents:Int, encertat:Boolean, missatges: List<String>){
    var color:String

    print(colors["blanc"])
    for (i in 1..71) print(" ")
    println(colors["reset"])
    print(colors["blanc"] + "  ${missatges[17]}")
    for (i in 1..56) print(" ")
    println(colors["reset"])
    print(colors["blanc"])
    for (i in 1..71) print(" ")
    println(colors["reset"])
    print(colors["blanc"] + "  ")
    for (i in 1..4) print(colors["negre"] + "                " + colors["blanc"] + " ")
    println(" " + colors["reset"])

    print(colors["blanc"] + "  " + colors["grisClar"] + "       ")
    if (jugades[0] < 10.00) print("\u001B[38;2;60;168;63m\u001B[1m${jugades[0].toInt()} ")
    else print("\u001b[1m\u001b[38;2;60;168;63m${jugades[0].toInt()}")
    print(colors["grisClar"] + "       ")

    print(colors["blanc"] + " " + colors["grisClar"] + "      ")
    if (((jugades[0]-jugades[7])/jugades[0]*100).toInt() < 10) print("\u001B[38;2;60;168;63m\u001B[1m  ${((jugades[0]-jugades[7])/jugades[0]*100).toInt()}")
    else if (((jugades[0]-jugades[7])/jugades[0]*100).toInt() < 100) print("\u001B[38;2;60;168;63m\u001B[1m ${((jugades[0]-jugades[7])/jugades[0]*100).toInt()}")
    else print("\u001b[1m\u001b[38;2;60;168;63m${((jugades[0]-jugades[7])/jugades[0]*100).toInt()}")
    print(colors["grisClar"] + "       ")

    print(colors["blanc"] + " " + colors["grisClar"] + "       ")
    if (ratxaActual < 10) print("\u001B[38;2;60;168;63m\u001B[1m$ratxaActual ")
    else print("\u001b[1m\u001b[38;2;60;168;63m$ratxaActual")
    print(colors["grisClar"] + "       ")

    print(colors["blanc"] + " " + colors["grisClar"] + "       ")
    if (ratxaMax < 10) print("\u001B[38;2;60;168;63m\u001B[1m$ratxaMax ")
    else print("\u001b[1m\u001b[38;2;60;168;63m$ratxaMax")
    print(colors["grisClar"] + "       ")
    println(colors["blanc"] + "  " + colors["reset"])

    print(colors["blanc"] + "  " + colors["grisClar"] + "    ${missatges[18]}    ")
    print(colors["blanc"] + " " + colors["grisClar"] + "   ${missatges[19]}   ")
    print(colors["blanc"] + " " + colors["grisClar"] + "${missatges[20]}")
    print(colors["blanc"] + " " + colors["grisClar"] + "  ${missatges[21]}  ")
    println(colors["blanc"] + "  " + colors["reset"])

    print(colors["blanc"] + "  ")
    for (i in 1..4) print(colors["negre"] + "                " + colors["blanc"] + " ")
    println(" " + colors["reset"])
    print(colors["blanc"])
    for (i in 1..71) print(" ")
    println(colors["reset"])

    for (i in 1..6){
        val max = maxOf(jugades[1], jugades[2], jugades[3], jugades[4], jugades[5], jugades[6])
        color = if (i == intents && encertat) colors["verd"].toString()
        else colors["gris"].toString()
        print("\u001B[48;2;255;255;255m\u001B[38;2;0;0;0m  $i ")
        if (jugades[i] == 0.00) {
            print(color + " 0  " + colors["blanc"])
            for (j in 1..61) print(" ")
        }
        else {
            for (j in 1..((jugades[i]/max*65).toInt() - 2)) print(color + " ")
            print(color + "${jugades[i].toInt()} ")
            for (j in 1..(65 - (jugades[i]/max*65).toInt())) print(colors["blanc"] + " ")
        }
        println(colors["blanc"] + "  " + colors["reset"])
    }

    print(colors["blanc"])
    for (i in 1..71) print(" ")
    println(colors["reset"])
}