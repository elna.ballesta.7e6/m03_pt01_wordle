import kotlin.io.path.Path
import kotlin.io.path.exists
import kotlin.io.path.writeText

/**
 * Programa que recrea el joc wordle
 * @author Elna Ballesta
 * @version 1.0
 */

/**
 * Mapa que guarda els codis dels colors que utilitza el programa
 */
val colors = mapOf("verd" to "\u001B[48;2;60;168;63m\u001B[1m\u001B[38;2;255;255;255m",
    "gris" to "\u001b[48;2;120;124;126m\u001B[1m\u001B[38;2;255;255;255m",
    "groc" to "\u001b[48;2;214;154;41m\u001B[1m\u001B[38;2;255;255;255m",
    "reset" to "\u001b[0m", "Lvermell" to "\u001b[38;2;255;0;0m",
    "negreFons" to "\u001b[48;2;0;0;0m\u001B[1m\u001B[38;2;255;255;255m",
    "grisClar" to "\u001b[0m\u001b[48;2;211;214;218m\u001B[38;5;0m",
    "negre" to "\u001b[48;2;211;214;218m\u001B[1m\u001B[38;5;0m",
    "blanc" to "\u001B[48;2;255;255;255m\u001B[1m\u001B[38;2;0;0;0m")

/**
 * Funció principal del programa
 */
fun main() {
    var idioma = "cat"
    var missatges = importarIdioma(idioma)
    var usuari = llegirUsuari(missatges)
    var fitxerUsuari =  Path("fitxers/estadistiques/generals.txt")
    if (!fitxerUsuari.exists()) {
        fitxerUsuari.writeText("0;0;0;0;0,0,0,0,0,0,0")
    }
    do {
        mostrarMenu(missatges)
        var opcio = readln()
        when (opcio) {
            "1" -> jugada(idioma, usuari, missatges)
            "2" -> {
                idioma = canviarIdioma(missatges)
                missatges = importarIdioma(idioma)
            }
            "3" -> usuari = llegirUsuari(missatges)
            "4" -> mostrarHistorial(missatges)
            "0" -> println(missatges[8])
            else -> println(missatges[7])
        }
    } while(opcio != "0")
    println("\n${missatges[10]}")
}
