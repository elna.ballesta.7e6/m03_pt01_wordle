import java.io.File
import java.nio.file.Path
import java.text.SimpleDateFormat
import java.util.*
import kotlin.io.path.*

/**
 * Funció que demana el nom de l'usuari fins que s'introdueix un nom d'usuari vàlid
 * @param missatges Llista que guarda els missatges que s'imprimeixen durant el programa
 * @return Nom de l'usuari
 */
fun demanarUsuari(missatges: List<String>): String {
    var usuari:String
    var fitxer = Path("fitxers/estadistiques/generals.txt")
    do {
        if(!fitxer.exists()) println(missatges[35])
        println("${missatges[22]}:")
        usuari = readln()
        fitxer = Path("fitxers/estadistiques/usr_$usuari.txt")
    }while (!fitxer.exists())
    return("usr_$usuari")
}

/**
 * Funció que mostrar l'historial de paraules d'un usuari
 * @param usuari Nom de l'usuari
 * @param missatges Llista que guarda els missatges que s'imprimeixen durant el programa
 */
fun historialParaules(usuari:String, missatges: List<String>) {
    var fitxerUsuari =  Path("fitxers/estadistiques/$usuari.txt")
    var partides = fitxerUsuari.readLines()
    for (i in 1 until partides.size) {
        var informacio = partides[i].split(";")
        print("${missatges[36]}: ${informacio[0]}   ${missatges[37]}: ${informacio[1]}   ")
        if (informacio[2].toInt() < 7) println("${missatges[38]}: ${informacio[2]}")
        else println(missatges[39])
    }
}

/**
 * Funció que importa els missatges del programa d'un fitxer a una llista
 * @param idioma Idioma en què s'està executant el programa
 * @return Llista que guarda els missatges que s'imprimeixen durant el programa
 */
fun importarIdioma(idioma:String): List<String> {
    var fitxer = Path("fitxers/missatges/$idioma.txt")
    var missatges = fitxer.readLines().toMutableList()
    for (i in missatges.indices) {
        missatges[i] = missatges[i].replace("*", "")
    }
    return(missatges.toList())
}

/**
 * Funció que llegeix el fitxer amb les paraules del diccionari i les guarda en una llista
 * @param idioma Idioma en què s'està executant el programa
 * @return Llista amb les paraules del diccionari
 */
fun obtenirDiccionari(idioma: String): List<String> {
    var diccionari: List<String> = listOf()
    val fitxer = File("./fitxers/diccionaris/$idioma.txt")
    fitxer.forEachLine{
        diccionari += it.uppercase(Locale.getDefault())
    }
    return (diccionari)
}

/**
 * Funció que guarda les dades de la partida al fitxer de l'usuari corresponent
 * @param usuari Nom de l'usuari
 * @param intents Nombre d'intents amb els quals s'ha endevinat la paraula
 * @param encertat Si la paraula s'ha encertat o no
 * @param paraula String amb la paraula que s'ha d'endivinar
 */
fun guardarEstadistiques(usuari:String, intents:Int, encertat:Boolean, paraula:String) {
    var fitxerUsuari =  Path("fitxers/estadistiques/$usuari.txt")
    var estadistiques = fitxerUsuari.readLines().toMutableList()
    var general = estadistiques[0].split(";").toMutableList()
    general[0] = "${general[0].toInt() + 1}"            //jugades + 1
    if(encertat) {
        general[1] = "${general[1].toInt() + 1}"        //exits + 1
        general[2] = "${general[2].toInt() + 1}"        //ratxa actual + 1
        if (general[2].toInt() > general[3].toInt()) { general[3]=general[2] }      //Actualitzar ratxa màxima
    } else {
        general[2] = "0"            //ratxa actual = 0
    }
    var arrayIntents = general[4].split(",").toMutableList()
    arrayIntents[intents-1] = "${arrayIntents[intents-1].toInt() + 1}"
    general[4] = arrayIntents.joinToString(separator = ",")
    estadistiques[0] = general.joinToString(separator=";")
    fitxerUsuari.writeText(estadistiques.joinToString(separator = "\n"))
    val data = SimpleDateFormat("yyyy/MM/dd").format(Date())
    fitxerUsuari.appendText("\n$data;$paraula;$intents")
}

/**
 * Funció que obté les dades de les estadístiques d'un usuari
 * @param usuari Nom de l'usuari
 * @return Array amb el nombre de paraules endevinades en x intents. Sent x cada posició de l'array
 */
fun obtenirEstadistiques(usuari: String): Array<Double> {
    var fitxerUsuari =  Path("fitxers/estadistiques/$usuari.txt")
    var estadistiques = fitxerUsuari.readLines()[0].split(";")
    var intents = estadistiques[4].split(",")
    var jugades = arrayOf(estadistiques[0], intents[0], intents[1], intents[2], intents[3], intents[4], intents[5], intents[6], estadistiques[2], estadistiques[3])
    return(jugades.map { it.toDouble() }.toTypedArray())
}