import kotlin.io.path.Path
import kotlin.io.path.exists
import kotlin.io.path.writeText

/**
 * Funció que mostra el menú principal
 * @param missatges Llista que guarda els missatges que s'imprimeixen durant el programa
 */
fun mostrarMenu(missatges:List<String>) {
    for (i in 0..30) println("\n")
    println("""~~~~~${missatges[0]}~~~~~
1. ${missatges[1]}
2. ${missatges[2]}
3. ${missatges[3]}
4. ${missatges[4]}
0. ${missatges[5]}
    
${missatges[6]}:
    """.trimIndent())
}

/**
 * Funció que executa la jugada al joc del wordle
 * @param idioma Idioma en què s'està executant el programa
 * @param usuari Nom de l'usuari
 * @param missatges Llista que guarda els missatges que s'imprimeixen durant el programa
 */
fun jugada(idioma:String, usuari:String, missatges:List<String>) {
    val diccionari = obtenirDiccionari(idioma)
    var paraules = diccionari
    var intent:String
    var tornarJugar:String
    var teclat = arrayOf(" Q ", " W ", " E ", " R ", " T ", " Y ", " U ", " I ", " O ", " P ", " A ", " S ", " D ", " F ", " G ", " H ", " J ", " K ", " L ", " Ç ", " Z ", " X ", " C ", " V ", " B ", " N ", " M ")

    do {
        var intents = 0
        var encertat = false
        var jugada = Array(6){Array(5){"\u001b[1m\u001b[38;5;0m\u001b[48;2;255;255;255m - "} }
        val paraula = paraules.random()
        println(paraula)
        paraules -= paraula
        teclat = reiniciarTeclat(teclat)
        imprimirTaulerJoc(teclat, jugada, missatges)
        do {        //Bucle que es repeteix per cada intent llegit
            println("" + colors["reset"])
            intent = readLine().toString().uppercase()
            if (intent.length == 5) {       //Controlar que la paraula té 5 lletres
                if (intent in diccionari) {     //Controlar la paraula és al diccionari
                    if (intent == paraula) {
                        encertat = true
                    }
                    var jugadaTeclat = Pair(arrayOf(teclat), jugada)
                    jugadaTeclat = comprovarParaula(paraula, intent, intents, jugadaTeclat)
                    jugada = jugadaTeclat.second
                    teclat = jugadaTeclat.first[0]
                    imprimirTaulerJoc(teclat, jugada, missatges)
                    intents++
                }
                else println(colors["Lvermell"] + missatges[11])
            }
            else println(colors["Lvermell"] + missatges[12] + colors["reset"])
        } while (intents < 6 && !encertat)
        if(!encertat) intents++
        imprimirMissatgesFinalPartida(encertat, paraula, missatges)
        guardarEstadistiques("generals", intents, encertat, paraula)
        guardarEstadistiques(usuari, intents, encertat, paraula)
        var jugades = obtenirEstadistiques(usuari)
        imprimirEstadistiques(jugades, jugades[8].toInt(), jugades[9].toInt(), intents, encertat, missatges)
        tornarJugar = preguntarTornarAJugar(paraules, missatges)
    } while (tornarJugar == "s" || tornarJugar == "S")
}

/**
 * Funció que mostra els idiomes possibles i retorna l'escollit
 * @param missatges Llista que guarda els missatges que s'imprimeixen durant el programa
 * @return Idioma escollit
 */
fun canviarIdioma(missatges: List<String>):String {
    var idiomes= arrayOf("cat", "esp", "eng")
    var opcio:String
    do {
        println("""~~~${missatges[23]}~~~
1. ${missatges[24]}
2. ${missatges[25]}
3. ${missatges[26]}

${missatges[6]}:
        """.trimIndent())
        opcio = readln()
        when (opcio) {
            "1" -> println(missatges[27])
            "2" -> println(missatges[28])
            "3" -> println(missatges[29])
            else -> println(missatges[7])
        }

    } while(opcio != "1" && opcio != "2" && opcio != "3")
    return (idiomes[opcio.toInt()-1])
}

/**
 * Funció que demana el nom de l'usuari que juga i el retorna
 * @param missatges Llista que guarda els missatges que s'imprimeixen durant el programa
 * @return Nom de l'usuari
 */
fun llegirUsuari(missatges: List<String>):String {
    var usuari:String
    println("${missatges[22]}:")
    usuari = readln()
    usuari = "usr_$usuari"
    //control nom d'usari
    var fitxerUsuari =  Path("fitxers/estadistiques/$usuari.txt")
    if (!fitxerUsuari.exists()) {
        fitxerUsuari.writeText("0;0;0;0;0,0,0,0,0,0,0")
    }
    return (usuari)
}

/**
 * Funció que gestiona l'opció historial del menú principal
 * @param missatges Llista que guarda els missatges que s'imprimeixen durant el programa
 */
fun mostrarHistorial(missatges: List<String>){
    var opcio:String
    do {
        println("""~~~${missatges[30]}~~~
1. ${missatges[31]}
2. ${missatges[32]}
3. ${missatges[33]}
4. ${missatges[34]}
0. ${missatges[9]}

${missatges[6]}:
        """.trimIndent())
        opcio = readln()
        when (opcio) {
            "1" -> {
                var jugades = obtenirEstadistiques("generals")
                imprimirEstadistiques(jugades, jugades[8].toInt(), jugades[9].toInt(), 0, false, missatges)
            }
            "2" -> {
                var usuari = demanarUsuari(missatges)
                var jugades = obtenirEstadistiques(usuari)
                imprimirEstadistiques(jugades, jugades[8].toInt(), jugades[9].toInt(), 0, false, missatges)
            }
            "3" -> historialParaules("generals", missatges)
            "4" -> {
                var usuari = demanarUsuari(missatges)
                historialParaules(usuari, missatges)
            }
            "0" -> println(missatges[9])
            else -> println(missatges[7])
        }
    } while(opcio != "0")
}