# M03_PT01 Wordle

Aquest projecte consisteix a desenvolupar una còpia del popular joc Wordle. En el meu cas, m'he basat en la versió catalana El Mot (WordleCAT) i l'he intentat reproduir el més exacte possible, tan pel que fa a funcionament com pel que fa a estètica. 

## Continguts
- [Funcionament](#funcionament)
- [Estructura](#estructura)
- [Documentació](#documentació)
- [Tests](#tests)

## Funcionament
Inicialment, el programa demana a l'usuari que introdueixi el nom i seguidament es mostra el menú principal. Per defecte, el programa està en català.

![Imatge menu principal](imatges/menuPrincipal.png)

A partir d'aquest menú, l'usuari pot escollir una de les opcions, i fins que no s'esculli sortir del programa, aquest no finalitzarà.

### Jugar

En escollir l'opció de jugar, el joc mostra el tauler buit perquè no s’ha introduït cap paraula. Llavors es pot introduir una paraula, que ha de ser de 5 lletres obligatòriament i ha d’estar al diccionari català. Si una d’aquestes dues coses no passen apareixen missatges d’error i s’ha d’introduir una altre paraula.

![Imatge tauler inicial](imatges/taulerInicial.png)
![Imatge missatge error](imatges/missatgeError1.png)
![Imatge missatge error](imatges/missatgeError2.png)


Un cop la paraula introduïda és correcte, es torna a mostrar el tauler amb la paraula intentada amb les lletres pintades del color corresponent. De la mateixa manera, es pinten les lletres del teclat (que no és funcional, no es pot pulsar les lletres). Es repeteix aquest procés fins que s’endevina la paraula o s’arriba als 6 intents. En tots dos casos apareix un missatge seguit de les estadístiques de les partides jugades fins el moment.

![Imatge tauler](imatges/tauler1.png)
![Imatge tauler](imatges/tauler2.png)
![Imatge tauler](imatges/tauler3.png)


Les estadístiques són les mateixes que al joc original. Les barres del nombre de partides encertades en cada intent s'escala en funció de les quantitats.

![Imatge estadístiques](imatges/estadistiques1.png)
![Imatge estadístiques](imatges/estadistiques2.png)

Després de mostrar les estadístiques, el programa pregunta a l'usuari si vol seguir jugant, o vol tornar al menú principal.

### Canviar idioma

En escollir l'opció de canviar idioma, es mostra un menú amb els idiomes possibles i es demana a l'usuari que n'esculli un. 

![Imatge menu canvi idioma](imatges/menuIdioma.png)

En canviar d'idioma, el programa torna al menú principal, que, igual que la resta de menús, passa a estar en l'idioma escollit.

![Imatge menu principal anglès](imatges/menuPrincipalAngles.png)

### Canviar usuari

En escollir l'opció de canviar usuari, es demana un nou nom d'usuari. Un cop introduit, el programa torna al menú principal.

### Mostrar historial

En escollir l'opció de mostrar historial, es mostra el menú amb les opcions.

![Imatge menu mostrar historial](imatges/menuHistorial.png)

Les opcions de mostrar les estadístiques mostren les mateixes estadístiques que el joc. Les opcions de mostrar historial de paraules mostren tots els registres de paraules jugades.
Les opcions generals mostren el total de totes les partides jugades. En canvi, les opcions d'un usuari demanen un nom d'usuari, i, si aquest és vàlid, mostren les partides corresponents a aquest usuari.

## Estructura
El programa està desenvolupat en llenguatge kotlin, i en un tipus de programació modular. Consta de quatre fitxers, el fitxer Main.kt amb la unció main.
El fitxer Utilitats.kt conté les funcions:
- reiniciarTeclat
- comprovarParaula
- canviarColorLletraTeclat
- preguntarTornarAJugar
- imprimirTaulerJoc
- imprimirMissatgesFinalsPartida
- imprimirEstadistiques

El fitxer OpcionsMenu.kt conté les funcions:
- mostrarMenu
- jugada
- canviarIdioma
- llegirUsuari
- mostrarHistorial

El fitxer Utilitats_fitxers.kt conté les funcions:
- demanarUsuari
- historialParaules
- importarIdioma
- obtenirDiccionari
- guardarEstadistiques
- obtenirEstadistiques

## Documentació
El projecte està documentat utilitzant l'eina Dokka, que utilitza el llenguatge KDoc. 
De totes les funcions estan comentats els arguments, el valor de retorn i una petita descripció del que fa.

[Documentació en format html](build/dokka)

## Tests
Per tal de testejar el projecte he realitzat un seguit de tests unitaris utilitzant l’eina JUnit5. 

He realitzat els tests següents a les funcions:
- preguntarTornarAJugar
    - testLlistaBuida
- reiniciarTeclat
    - testReiniciarTeclat
- comprovarParaula
    - testLletresRepetides
- canviarColorLletraTeclat
    - testLletraVerd
    - testLletraGrogaInicialmentVerda
    - testLletraGroga
    - testLletraGrisInicialmentGroga
    - testLletraGris




