WORDLE  CAT
Jugar
Canviar idioma
Canviar usuari
Mostrar historial
Sortir del programa
Selecciona una opció
Opció no vàlida
Sortint del programa
Tornar al menú principal
GRÀCIES PER JUGAR!!
Paraula incorrecte. La paraula ha d'estar al diccionari de la llengua catalana.
Paraula incorrecte. Introdueix una paraula de 5 lletres.
No queden més paraules per endevinar. Pots escollir un altre idioma
Vols tornar a jugar? (Tornar a jugar: S | Tornar al menú principal: (qualsevol altre cosa))
ENHORABONA!!
Solució:
Estadístiques
Jugades *
 Èxits (%)
  Ratxa Actual  *
Ratxa Màxima
Introdueix el nom d'usuari
IDIOMES
Català
Castellà
Anglès
Ha escollit jugar en català
Ha escollit jugar en castellà
Ha escollit jugar en anglès
HISTORIAL
Mostrar estadístiques generals
Mostrar estadístiques d'un usuari
Mostrar historial de paraules general
Mostrar historial de paraules d'un usuari
Error. Aquest usuari no existeix
Dia
Paraula
Intents
No encertada