WORDLE  ENG
Play
Change language
Change user
Show history
Exit program
Select an option
Invalid option
Exiting program
Return to main menu
THANKS FOR PLAYING!!
Wrong word. The word must be in the dictionary of the english language.
Wrong word. Enter a 5-letter word.
No more words left to guess. You can choose another language
Do you want to play again? (Replay: Y | Return to main menu: (anything else))
CONGRATULATIONS!!
Solution:
Statistics   *
 Played *
  Win (%) *
 Current Streak *
 Max Streak *
Enter username
LANGUAGES
Catalan
Spanish
English
You have chosen to play in Catalan
You have chosen to play in Spanish
You have chosen to play in English
HISTORY
Show general statistics
Show statistics for a user
Show general word history
Show word history for a user
Error. This user does not exist
Day
Word
Attempts
Not guessed